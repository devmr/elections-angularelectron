# Elections-mg - web app

Web app - powered by Angular 7 and ElectronJs of the website https://elections-mg.com

## Core tools

- [Angular](https://angular.io/) v7.3
- [Electron](https://electronjs.org/) v4.0

### Angular package

- [@angular/material](https://material.angular.io) - v7.3
- [@angular/cdk](https://material.angular.io/cdk) - v7.3
- @angular/animations - v7.2
- @angular/flex-layout - v8.0

### Electron package

### Other Libraries tools

- [hammerjs](http://hammerjs.github.io/) - v2.0

### Thanks

- https://github.com/maximegris/angular-electron

