import { Injectable } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ToolbarsettingsService {

  toolbarShow: Observable<boolean>;

  constructor(private router: Router) {
    this.toolbarShow = this.router.events
      .filter(event => {
        return event instanceof NavigationEnd;
      })

      .map((event: NavigationEnd) => {
        const route: any = this.router.config.find(r => {
          return '/' + r.path === event.url.split('?')[0];
        });

        return route.toolbar ? route.toolbar : false;
      });
  }
}
