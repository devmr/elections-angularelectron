import { TestBed } from '@angular/core/testing';

import { ToolbarsettingsService } from './toolbarsettings.service';

describe('ToolbarsettingsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ToolbarsettingsService = TestBed.get(ToolbarsettingsService);
    expect(service).toBeTruthy();
  });
});
